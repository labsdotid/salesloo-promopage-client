<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.fiqhidayat.com
 * @since             1.0.0
 * @package           Salesloo_Promopage_Client
 *
 * @wordpress-plugin
 * Plugin Name:       Salesloo Promopage Client
 * Plugin URI:        https://www.salesloo.com
 * Description:       Client plugin for salesloo Promopage plugin
 * Version:           1.0.5
 * Author:            Salesloo Dev Team
 * Author URI:        https://www.salesloo.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       salesloo-promopage-client
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('SPPC_VERSION', '1.0.5');
define('SPPC_URL', plugin_dir_url(__FILE__));
define('SPPC_PATH', plugin_dir_path(__FILE__));
define('SPPC_ROOT', __FILE__);

require 'update-checker/plugin-update-checker.php';

/**
 * The code that runs during plugin activation.
 */
function activate_salesloo_promopage_client()
{
    require_once SPPC_PATH . 'includes/activator.php';
    \Salesloo_Promopage_Client\Activator::run();
}

/**
 * The code that runs during plugin deactivation.
 */
function deactivate_salesloo_promopage_client()
{
    require_once SPPC_PATH . 'includes/deactivator.php';
    Salesloo_Promopage_Client\Deactivator::run();
}

register_activation_hook(__FILE__, 'activate_salesloo_promopage_client');
register_deactivation_hook(__FILE__, 'deactivate_salesloo_promopage_client');


add_action('plugins_loaded', 'salesloo_promopage_client_load_plugin_textdomain');

function salesloo_promopage_client_load_plugin_textdomain()
{
    load_plugin_textdomain(
        'salesloo-abc',
        false,
        dirname(plugin_basename(__FILE__)) . '/languages'
    );
}

require SPPC_PATH . 'includes/plugin.php';
\Salesloo_Promopage_Client\Plugin::run();

if (!function_exists('__debug')) {
    function __debug()
    {
        echo '<pre>';
        print_r(func_get_args());
        echo '</pre>';
    }
}
