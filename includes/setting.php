<?php

namespace Salesloo_Promopage_Client;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * Setting
 */
class Setting
{

    /**
     * Instance.
     *
     * Holds the setting instance.
     *
     * @since 1.0.0
     * @access public
     */
    public static $instance = null;

    /**
     * Init.
     *
     * @since 1.0.0
     */
    public static function init()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Register Menu
     * 
     * register system info menu
     */
    public function register_menu()
    {
        add_submenu_page(
            'promopage',
            __('Setting', 'salesloo-promopage'),
            __('Setting', 'salesloo-promopage'),
            'manage_options',
            'promopage-setting',
            [$this, 'page'],
        );
    }

    public function page()
    {
?>
        <div class="wrap">
            <h2><?php _e('Promo Page Generator', 'salesloo-promopage'); ?></h2>

            <form action="" method="post" enctype="multipart/form-data">
                <table class="form-table">
                    <tbody>
                        <tr>
                            <th scope="row">
                                <label>Base</label>
                            </th>
                            <td>
                                <?php echo site_url(); ?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <label>Auth Token</label>
                            </th>
                            <td>
                                <input type="text" id="sppcApiAuth" name="sppc_api_token" value="<?php echo get_option('sppc_api_token'); ?>" class="regular-text" readonly>&nbsp;<button type="button" class="button" id="sppcGenerateAuth">Regenerate</button>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <label>Posts Types</label>
                            </th>
                            <td>
                                <?php

                                $post_types = get_post_types([
                                    'public' => true,
                                ], 'objects');

                                $exclude_post_types = [
                                    'attachment',
                                    'elementor_library',
                                    'promopage'
                                ];

                                $post_types_values = (array) get_option('sppc_post_types');

                                foreach ((array)$post_types as $post) {

                                    if (in_array($post->name, $exclude_post_types)) continue;
                                ?>
                                    <label>
                                        <input name="sppc_post_types[]" type="checkbox" value="<?php echo $post->name;  ?>" <?php if (in_array($post->name, $post_types_values)) echo 'checked="checked"'; ?> />
                                        <span><?php echo $post->label;  ?></span>
                                    </label><br /><br />
                                <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <label>&nbsp;</label>
                            </th>
                            <td>
                                <input type="submit" name="save" class="button button-primary" value="Save Changes">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        let sppcGenerateToken = function() {
                            let key = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2,
                                15);
                            jQuery('#sppcApiAuth').val(key);
                        }

                        jQuery('#sppcGenerateAuth').on('click', function() {
                            sppcGenerateToken();
                        });

                        let currentValue = jQuery('#sppcApiAuth').val();

                        if (!currentValue) {
                            sppcGenerateToken();
                        }
                    })
                </script>
                <?php wp_nonce_field('sppc_nonce', '__nonce'); ?>
            </form>
        </div>
<?php
    }

    public function save()
    {

        if (isset($_POST['save']) && isset($_POST['__nonce'])) :
            if (wp_verify_nonce($_POST['__nonce'], 'sppc_nonce')) :

                unset($_POST['save']);
                unset($_POST['__nonce']);
                unset($_POST['_wp_http_referer']);

                foreach ($_POST as $key => $value) {
                    \update_option($key, $value);
                }

                add_action('admin_notices', function () {
                    echo '<div id="message" class="updated notice notice-success"><p><strong>' . __('Your settings have been saved.', 'salesloo-salespop') . '</strong></p></div>';
                });
            endif;
        endif;
    }



    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        add_action('admin_menu', [$this, 'register_menu']);
        add_filter('admin_init', [$this, 'save']);
    }
}
