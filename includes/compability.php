<?php

namespace Salesloo_Promopage_Client;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * Compability
 */
class Compability
{

    /**
     * Instance.
     *
     * Holds the compability instance.
     *
     * @since 1.0.0
     * @access public
     */
    public static $instance = null;

    /**
     * Init.
     *
     * @since 1.0.0
     */
    public static function init()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function after_setup_theme()
    {
        if (did_action('elementor/loaded')) :
            add_action('elementor/init', [$this, 'elementor_init']);
            add_action('elementor/widgets/widgets_registered', [$this, 'elementor_widget']);
        endif;
    }

    public function elementor_init()
    {
        \Elementor\Plugin::instance()->elements_manager->add_category(
            'promopage',
            [
                'title'  => 'Promopage',
                'icon' => 'font'
            ],
            1
        );
    }

    /**
     * elementor widget
     * @return [type] [description]
     */
    public function elementor_widget()
    {
        require_once SPPC_PATH . '/includes/elementor-widget/image.php';
        require_once SPPC_PATH . '/includes/elementor-widget/whatsapp.php';
    }



    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        add_action('after_setup_theme', [$this, 'after_setup_theme']);
    }
}
