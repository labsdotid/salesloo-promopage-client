<?php

namespace Salesloo_Promopage_Client;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * Post type
 */
class Post_Type
{

    /**
     * Instance.
     *
     * Holds the post type instance.
     *
     * @since 1.0.0
     * @access public
     */
    public static $instance = null;

    /**
     * Init.
     *
     * @since 1.0.0
     */
    public static function init()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function create()
    {
        register_post_type(
            'promopage',
            array(
                'labels' => array(
                    'name'               => __('Promopages', 'salesloo-promopage'),
                    'singular_name'      => __('Promopage', 'salesloo-promopage'),
                    'add_new'            => __('Add New', 'salesloo-promopage'),
                    'add_new_item'       => __('Add New Promopage', 'salesloo-promopage'),
                    'edit'               => __('Edit', 'salesloo-promopage'),
                    'edit_item'          => __('Edit Promopage', 'salesloo-promopage'),
                    'new_item'           => __('New Promopage', 'salesloo-promopage'),
                    'view'               => __('View Promopage', 'salesloo-promopage'),
                    'view_item'          => __('View Promopage', 'salesloo-promopage'),
                    'search_items'       => __('Search Promopage', 'salesloo-promopage'),
                    'not_found'          => __('No Promopages found', 'salesloo-promopage'),
                    'not_found_in_trash' => __('No Promopages found in Trash', 'salesloo-promopage')
                ),
                'public' => false,
                'publicly_queryable' => true,
                'show_ui' => true,
                'hierarchical' => false,
                'show_in_menu' => 'promopage',
                'has_archive' => false,
                'supports' => array(
                    'title',
                ),
                'can_export' => false,
                'rewrite' =>  false
            )
        );
    }


    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        add_action('init', [$this, 'create']);
    }
}
