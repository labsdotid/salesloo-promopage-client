<?php
function salesloo_promopage_client_default_shortcodes()
{
    $shortcodes = [
        'avatar' => [
            'label' => __('Avatar', 'salesloo-promopage-client'),
            'type' => 'image',
            'value' => SPPC_URL . 'assets/images/jhondoe.jpg',
            'default' => true
        ],
        'first_name' => [
            'label' => __('First Name', 'salesloo-promopage-client'),
            'type' => 'text',
            'value' => 'Jhon',
            'default' => true
        ],
        'last_name' => [
            'label' => __('Last Name', 'salesloo-promopage-client'),
            'type' => 'text',
            'value' => 'Doe',
            'default' => true
        ],
        'pixels' => [
            'label' => __('Facebook Pixel Ids', 'salesloo-promopage-client'),
            'type' => 'fb_pixel_id',
            'value' => '',
            'default' => true
        ],
        'link_affiliate' => [
            'label' => __('Link Affiliate', 'salesloo-promopage-client'),
            'type' => 'link_affiliate',
            'value' => '',
            'default' => true
        ]
    ];

    return $shortcodes;
}


function salesloo_promopage_client_dropdown_posts($args = '')
{

    $defaults = array(
        'selected'              => FALSE,
        'pagination'            => FALSE,
        'posts_per_page'        => -1,
        'post_status'           => 'publish',
        'cache_results'         => TRUE,
        'cache_post_meta_cache' => TRUE,
        'echo'                  => 1,
        'select_name'           => 'post_id',
        'id'                    => '',
        'class'                 => '',
        'show'                  => 'post_title',
        'show_callback'         => NULL,
        'show_option_all'       => NULL,
        'show_option_none'      => NULL,
        'option_none_value'     => '',
        'multi'                 => FALSE,
        'value_field'           => 'ID',
        'order'                 => 'ASC',
        'orderby'               => 'post_title',
    );

    $r = wp_parse_args($args, $defaults);

    $posts  = get_posts($r);
    $output = '';

    $show = $r['show'];

    if (!empty($posts)) {

        $name = esc_attr($r['select_name']);

        if ($r['multi'] && !$r['id']) {
            $id = '';
        } else {
            $id = $r['id'] ? " id='" . esc_attr($r['id']) . "'" : " id='$name'";
        }

        $output = "<select name='{$name}'{$id} class='" . esc_attr($r['class']) . "' style='width:100%'>\n";

        if ($r['show_option_all']) {
            $output .= "\t<option value='0'>{$r['show_option_all']}</option>\n";
        }

        if ($r['show_option_none']) {
            $_selected = selected($r['show_option_none'], $r['selected'], FALSE);
            $output .= "\t<option value='" . esc_attr($r['option_none_value']) . "'$_selected>{$r['show_option_none']}</option>\n";
        }

        foreach ((array) $posts as $post) {

            $value   = !isset($r['value_field']) || !isset($post->{$r['value_field']}) ? $post->ID : $post->{$r['value_field']};
            $_selected = selected($value, $r['selected'], FALSE);

            $display = !empty($post->$show) ? $post->$show : sprintf(__('#%d (no title)'), $post->ID);

            if ($r['show_callback']) $display = call_user_func($r['show_callback'], $display, $post->ID);

            $output .= "\t<option value='{$value}'{$_selected}>" . esc_html($display) . "</option>\n";
        }

        $output .= "</select>";
    }

    /**
     * Filter the HTML output of a list of pages as a drop down.
     *
     * @since 1.0.0
     *
     * @param string $output HTML output for drop down list of posts.
     * @param array  $r      The parsed arguments array.
     * @param array  $posts  List of WP_Post objects returned by `get_posts()`
     */
    $html = apply_filters('wp_dropdown_posts', $output, $r, $posts);

    if ($r['echo']) {
        echo $html;
    }

    return $html;
}

function salesloo_promopage_client_get_shortcode($post_id)
{
    global $wp;

    $promopage_id = intval(get_post_meta($post_id, 'promopage_id', true));
    $shortcodes = [];
    if ($promopage_id) {
        $shortcodes = get_post_meta($promopage_id, 'promopage_shortcodes', true);

        if (isset($wp->query_vars['username'])) {
            $username = sanitize_text_field($wp->query_vars['username']);
            $affiliate_shortcodes = get_post_meta($promopage_id, 'promopage_shortcodes' . $username, true);

            $shortcodes = wp_parse_args($affiliate_shortcodes, $shortcodes);
        }
    }

    return $shortcodes;
}

add_action('init',  function () {
    add_rewrite_rule('([^/@]+)(?:/([0-9]+))?/(@[^/]+)?$', 'index.php?name=$matches[1]&page=$matches[2]&username=$matches[3]', 'top');
    add_rewrite_rule('([^/@]+)(?:/([0-9]+))?(@[^/]+)?$', 'index.php?name=$matches[1]&page=$matches[2]&username=$matches[3]', 'top');
    add_rewrite_rule('(@[^/]+)?$', 'index.php?name=$matches[1]&page=&username=$matches[1]', 'top');
});

add_filter('query_vars', function ($query_vars) {
    $query_vars[] = 'username';
    return $query_vars;
});


function salesloo_promopage_client_set_shortcode_value($wp)
{
    global $__saleslooabc;

    if (is_admin()) return;

    $__saleslooabc['shortcodes'] = salesloo_promopage_client_get_shortcode(get_the_ID());
}
add_action('wp', 'salesloo_promopage_client_set_shortcode_value');


add_action('parse_request', function ($wp) {

    if (is_admin()) return;

    if (isset($wp->query_vars['username']) && $wp->query_vars['username']) {
        if ($wp->query_vars['username'] == $wp->query_vars['name'] && get_option('show_on_front') == 'page' && get_option('page_on_front')) {
            $p = get_post(get_option('page_on_front'));
            if ($p) {
                $wp->set_query_var('name', $p->post_name);
            }
        }
    }
});



function salesloo_promopage_client_the_content($content)
{
    global $post;

    if (is_admin() || empty($content)) return $content;

    $content = preg_replace_callback('/{{(.*?)}}/', 'salesloo_promopage_client_replace_shortcode', $content);

    return $content;
}
add_filter('the_content', 'salesloo_promopage_client_the_content', 99);

function salesloo_promopage_client_wp_head()
{
    global $__saleslooabc;

    $shortcodes = isset($__saleslooabc['shortcodes']) ? $__saleslooabc['shortcodes'] : array();
    $link_affiliates = [];
    $pixel_ids = [];
    foreach ((array)$shortcodes as $shortcode => $val) {
        if (isset($val['value']) && $val['value']) {
            if ($shortcode == 'link_affiliate' || isset($val['type']) && $val['type'] == 'link_affiliate') {
                $link_affiliates[] = esc_url_raw($val['value']);
            }

            if ($shortcode == 'pixels' || isset($val['type']) && $val['type'] == 'fb_pixel_id') {
                $pixels = explode(',', sanitize_text_field($val['value']));
                if ($pixel_ids) {
                    $pixel_ids = wp_parse_args($pixels, $pixel_ids);
                } else {
                    $pixel_ids = $pixels;
                }
            }
        }
    }

    if ($link_affiliates) {
        foreach ($link_affiliates as $link_affiliate) {
            $link_affiliate = esc_url($link_affiliate);

            if (is_ssl()) :
                $link_affiliate = str_replace('http://', 'https://', $link_affiliate);
            endif;

            echo '<img width="1" height="1" style="opacity: 0;position:fixed;top:-10px;" src="' . $link_affiliate . '" />';
        }
    }

    if ($pixel_ids) :
        $script = "<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');";

        foreach ($pixel_ids as $pixel_id) :
            $pixel_id = sanitize_text_field($pixel_id);
            $script .= "fbq('init', '$pixel_id');";
        endforeach;
        $script .= "fbq('track', 'PageView');";
        $script .= "</script>";

        $script .= "<noscript>";
        foreach ($pixel_ids as $pixel_id) :
            $pixel_id = sanitize_text_field($pixel_id);
            $script .= '<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=' . $pixel_id . '&ev=PageView&noscript=1" />';
        endforeach;
        $script .= '</noscript>';

        echo $script;
    endif;
}
add_action('wp_head', 'salesloo_promopage_client_wp_head');

function salesloo_promopage_client_replace_shortcode($shortcode_array)
{
    global $__saleslooabc;

    $data        = isset($__saleslooabc['shortcodes']) ? $__saleslooabc['shortcodes'] : array();
    $shortcode   = $shortcode_array[1];

    $val = isset($data[$shortcode]['value']) && $data[$shortcode]['value'] ? esc_attr($data[$shortcode]['value']) : false;

    if ($val === false) return $shortcode_array[0];

    if (preg_match('/(^(?:[a-zA-Z0-9]+(?:\-*[a-zA-Z0-9])*\.)+[a-zA-Z]{2,7}$)/', $val, $match)) {
        $val = esc_url_raw($val);
    } else {
        $filtered_phone_number = filter_var($val, FILTER_SANITIZE_NUMBER_INT);
        $phone_to_check = str_replace('-', '', $filtered_phone_number);

        if ($phone_to_check && strlen($phone_to_check) > 8 && strlen($phone_to_check) < 15) {
            $phone_to_check  = preg_replace('/[^0-9]/', '', $phone_to_check);
            $phone_to_check  = preg_replace('/^620/', '62', $phone_to_check);
            $intl_phone_code = apply_filters('salesloo_promopage_client_intl_phone_code', '62');
            $phone_to_check  = preg_replace('/^0/', $intl_phone_code, $phone_to_check);

            $val = $phone_to_check;
        } else {
            $val = sanitize_text_field($val);
        }
    }

    return $val;
}


function salesloo_promopage_pages($promopage_id)
{
    global $wpdb;

    $q = $wpdb->prepare("select * from {$wpdb->postmeta} where meta_key = %s and meta_value = %d", 'promopage_id', $promopage_id);

    $results = $wpdb->get_results($q);

    $pages = [];
    foreach ((array)$results as $result) {
        $pages[] = $result->post_id;
    }

    return $pages;
}


function salesloo_promopgae_client_take_filter_celan_url($good_url, $original_url, $contect)
{
    $string = $original_url;
    if (preg_match('/{{(.*?)}}/', $string)) {
        return $original_url;
    }

    return $good_url;
}
add_filter('clean_url', 'salesloo_promopgae_client_take_filter_celan_url', 99, 3);
