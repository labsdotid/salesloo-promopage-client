<?php

namespace Salesloo_Promopage_Client;

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Salesloo_Promopage_Client
 * @subpackage Salesloo_Promopage_Client/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Activator
{

	/**
	 * Run
	 *
	 * @since    1.0.0
	 */
	public static function run()
	{
	}
}
