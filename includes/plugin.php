<?php

namespace Salesloo_Promopage_Client;

/**
 * The core plugin class.
 *
 * @since      1.0.0
 * @package    SPPC
 * @subpackage SPPC/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Plugin
{

	/**
	 * instance
	 */
	public static $instance;

	/**
	 * Instance.
	 *
	 * Ensures only one instance of the plugin class is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public static function run()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Define the core functionality of the plugin.
	 */
	public function __construct()
	{
		$this->load_dependencies();
		Admin::init();
		Setting::init();
		Metabox::init();
		Post_Type::init();
		Compability::init();
		Rest_Api::init();
	}

	/**
	 * Load the required dependencies for this plugin.
	 * 
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies()
	{
		require_once SPPC_PATH . 'includes/functions.php';

		require_once SPPC_PATH . 'includes/admin.php';
		require_once SPPC_PATH . 'includes/setting.php';
		require_once SPPC_PATH . 'includes/metabox.php';
		require_once SPPC_PATH . 'includes/post-type.php';
		require_once SPPC_PATH . 'includes/compability.php';
		require_once SPPC_PATH . 'includes/rest-api.php';
	}
}
