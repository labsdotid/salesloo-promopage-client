<?php

namespace Salesloo_Promopage_Client;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * Metabox
 */
class Metabox
{

    /**
     * Instance.
     *
     * Holds the metabox instance.
     *
     * @since 1.0.0
     * @access public
     */
    public static $instance = null;

    /**
     * Init.
     *
     * @since 1.0.0
     */
    public static function init()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function add()
    {
        $post_types = get_option('sppc_post_types');
        if ($post_types) {
            add_meta_box(
                'sppc_shortcodes',
                'Bonus page shortcodes',
                [$this, 'shortcode'],
                $post_types,
                'side',
                'high'
            );
        }
    }

    public function shortcode($post)
    {
        $promopage_id = get_post_meta($post->ID, 'promopage_id', true);
        salesloo_promopage_client_dropdown_posts(
            array(
                'post_type'        => 'promopage',
                'selected'         => $promopage_id,
                'select_name'      => 'promopage_id',
                'show_option_none' => __('-'),
                'sort_column'      => 'menu_order, post_title',
                'class'            => 'large-text'
            )
        );
    }

    public function promopage($post)
    {
        if ($post->post_type != 'promopage') return;

        $fields = get_post_meta($post->ID, 'promopage_shortcodes', true);
        $fields = wp_parse_args($fields, salesloo_promopage_client_default_shortcodes());

        $types = [
            'text' => 'Text',
            'image' => 'Image',
            'link_affiliate' => 'Link Affiliate',
            'fb_pixel_id' => 'FB pixel ID',
        ];

        wp_nonce_field('salesloo-abc', '__nonce', false);
?>
        <div class="sppc" style="margin: 15px 0;">
            <table class="sppc-input-box wp-list-table widefat fixed striped">
                <thead>
                    <tr class="sppc-shortcoe-field">
                        <td style="width:150px;position:relative">
                            Shortcode
                        </td>
                        <td style="width:150px;position:relative">
                            Label
                        </td>
                        <td style="width:110px;position:relative">
                            Type
                        </td>
                        <td>Default Value</td>
                        <td style="width:30px">
                        </td>
                    </tr>
                </thead>
                <tbody class="sppcshortcode">
                    <?php if ($fields) : ?>
                        <?php
                        $i = 0;
                        ?>
                        <?php foreach ((array) $fields as $key => $val) : ?>
                            <?php

                            $type = isset($val['type']) ? $val['type'] : 'text';
                            $readonly =  isset($val['default']) && $val['default'] ? 'readonly' : '';
                            $disabled =  isset($val['default']) && $val['default'] ? 'disabled' : '';
                            $default =  isset($val['default']) && $val['default'] ? 1 : 0;

                            if (in_array($key, ['link_affiliate', 'pixels'])) :
                            ?>
                                <input type="hidden" name="shortcodes[<?php echo $i; ?>][code]" value="<?php echo $key; ?>">
                                <input type="hidden" name="shortcodes[<?php echo $i; ?>][label]" value="<?php echo $type; ?>">
                                <input type="hidden" name="shortcodes[<?php echo $i; ?>][value]" value="<?php echo $val['value'] ?>">
                                <input type="hidden" name="shortcodes[<?php echo $i; ?>][default]" value="<?php echo $default; ?>">
                            <?php
                            else :
                            ?>
                                <tr class="sppc-shortcode-field">
                                    <td style="width:150px;position:relative">
                                        <input type="text" name="shortcodes[<?php echo $i; ?>][code]" placeholder="{{shortcode}}" value="{{<?php echo $key; ?>}}" <?php echo $readonly; ?>>
                                    </td>
                                    <td style="width:150px">
                                        <input type="text" name="shortcodes[<?php echo $i; ?>][label]" value="<?php echo $val['label']; ?>" placeholder="Label">
                                    </td>
                                    <td style="width:110px">
                                        <select name="shortcodes[<?php echo $i; ?>][type]" onchange="sppcSelectType(this, <?php echo $i; ?>);" style="width:100%" <?php echo $disabled; ?>>
                                            <?php foreach ($types as $k => $t) : ?>
                                                <option value="<?php echo $k; ?>" <?php echo $type == $k ? 'selected="selected"' : ''; ?>><?php echo $t; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <?php if ($default) : ?>
                                            <input type="hidden" name="shortcodes[<?php echo $i; ?>][type]" value="<?php echo $type; ?>">
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($type == 'image') : ?>
                                            <input class="sppcvalue" type="text" name="shortcodes[<?php echo $i; ?>][value]" placeholder="Default Value" value="<?php echo $val['value']; ?>" style="width: 80%">
                                            <button class="wpapgvalue" class="button sppcupload" type="button" onclick="sppcUploader(this)">Upload</button>
                                        <?php else : ?>
                                            <input style="width:100%" type="text" name="shortcodes[<?php echo $i; ?>][value]" placeholder="Default Value" value="<?php echo $val['value']; ?>">
                                        <?php endif; ?>
                                    </td>
                                    <td style="width:30px">
                                        <?php if (!isset($val['default']) || empty($val['default'])) : ?>
                                            <div style="text-align:center;height: 30px;line-height: 30px;">
                                                <span class="dashicons dashicons-trash sppc-shortcode-field-remove" style="cursor:pointer;margin-top: 9px;"></span>
                                            </div>
                                        <?php endif; ?>
                                    </td>

                                    <input type="hidden" name="shortcodes[<?php echo $i; ?>][default]" value="<?php echo $default; ?>">
                                </tr>
                            <?php
                                $i++;
                            endif;
                            ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align:right;width: 100%">
                            <button class="button add-more" type="button">Add field</button>
                        </td>
                        <td></td>
                    <tr>
                </tfoot>
            </table>

            <script type="text/javascript">
                jQuery(document).ready(function($) {

                    jQuery(".add-more").click(function() {
                        let field = jQuery('.sppcshortcode').find('tr.sppc-shortcode-field').length;

                        let html = '<tr class="sppc-shortcode-field">';
                        html += '<td style="width:150px"><input type="text" name="shortcodes[' + field + '][code]" placeholder="{{shortcode}}"></td>';
                        html += '<td style="width:150px"><input type="text" name="shortcodes[' + field + '][label]" placeholder="Label"></td>';
                        html += '<td style="width:110px"><select name="shortcodes[' + field + '][type]" style="width:100%" onchange="sppcSelectType(this, ' + field + ');"><option value="text">Text</option><option value="image">Image Upload</option><option value="link_affiliate">Link Affiliate</option><option value="fb_pixel_id">FB Pixel ID</option></select></td>'
                        html += '<td><input class="sppcvalue" style="width:100%" type="text" name="shortcodes[' + field + '][value]" placeholder="Default Value"></td>';
                        html += '<td style="width:30px">';
                        html += '<div style="text-align:center;height: 30px;line-height: 30px;">';
                        html += '<span class="dashicons dashicons-trash sppc-shortcode-field-remove" style="cursor:pointer;margin-top: 9px;"></span>';
                        html += '</div></td>';
                        html += '<input type="hidden" name="shortcodes[' + field + '][default]" value="0">';
                        html += '</tr>';
                        jQuery('tbody.sppcshortcode').append(html);
                    });

                    jQuery("body").on("click", ".sppc-shortcode-field-remove", function() {
                        jQuery(this).parents(".sppc-shortcode-field").remove();
                    });

                });

                function sppcCopy(ini) {
                    let input = jQuery(ini).parent().find('input');
                    input.select();
                    document.execCommand('copy');
                }

                function sppcSelectType(ini, field) {

                    let val = jQuery(ini).val();
                    let tr = jQuery(ini).parent().parent();
                    let valueField = tr.find('.sppcvalue');
                    let tdValueField = valueField.parent();
                    if (val == 'image') {
                        console.log(valueField);
                        valueField.css('width', '80%');
                        tdValueField.append('<button class="button sppcupload" type="button" onclick="sppcUploader(this)">Upload</button>');
                    } else {
                        tdValueField.empty();
                        tdValueField.append('<input class="sppcvalue" type="text" name="shortcodes[' + field + '][value]" placeholder="Default Shortcode Value">')
                    }
                }
            </script>
        </div>

<?php
    }

    public function post_meta_save($post_id, $post)
    {
        global $wpdb;

        // return if autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        // Return if the user doesn't have edit permissions.
        if (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }

        if ($post->post_type == 'promopage' && isset($_POST['__nonce'])) {
            if (!wp_verify_nonce($_POST['__nonce'], 'salesloo-abc')) return $post_id;

            $shortcodes = [];

            if (isset($_POST['shortcodes'])) {
                foreach ((array)$_POST['shortcodes'] as $val) {
                    if (empty($val['code'])) continue;
                    $code = str_replace('{{', '', $val['code']);
                    $code = str_replace('}}', '', $code);
                    unset($val['code']);
                    $shortcodes[$code] = $val;
                }
            }

            $shortcodes = wp_parse_args($shortcodes, salesloo_promopage_client_default_shortcodes());
            update_post_meta($post->ID, 'promopage_shortcodes', $shortcodes);
        }

        if (isset($_POST['promopage_id'])) {
            $promopage_id = intval($_POST['promopage_id']);
            update_post_meta($post->ID, 'promopage_id', $promopage_id);
        }
    }



    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        add_action('edit_form_after_editor', [$this, 'promopage']);
        add_action('add_meta_boxes', [$this, 'add']);
        add_action('save_post', [$this, 'post_meta_save'], 1, 2);
    }
}
