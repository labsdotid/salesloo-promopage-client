<?php

namespace Salesloo_Promopage_Client;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * Salesloo WP Rest Api Costan
 */
class Rest_Api
{
    private $namespace = 'sppc';

    /**
     * Instance.
     *
     * Holds the rest api instance.
     *
     * @since 1.0.0
     * @access public
     */
    public static $instance = null;

    /**
     * Init.
     *
     * @since 1.0.0
     */
    public static function init()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * register salesloo rest api routes
     */
    public function register_routes()
    {
        register_rest_route(
            $this->namespace,
            '/v1/promopages',
            [
                'methods'             => \WP_REST_Server::READABLE,
                'callback'            => [$this, 'get_promopages'],
                'permission_callback' => [$this, 'permissions'],
            ],
        );

        register_rest_route(
            $this->namespace,
            '/v1/promopage/(?P<id>\d+)',
            [
                'methods'             => \WP_REST_Server::READABLE,
                'callback'            => [$this, 'get_promopage'],
                'permission_callback' => [$this, 'permissions'],
            ],
        );

        register_rest_route(
            $this->namespace,
            '/v1/promopage/(?P<id>\d+)',
            [
                'methods'             => \WP_REST_Server::EDITABLE,
                'callback'            => [$this, 'update_promopage'],
                'permission_callback' => [$this, 'permissions'],
            ],
        );
    }

    public function permissions(\WP_REST_Request $request)
    {
        $auth = $request->get_header('Authorization');
        if ($auth) {
            $token = substr($auth, 7);

            if ($token == get_option('sppc_api_token')) {
                return true;
            }
        }

        return false;
    }

    public function get_promopages(\WP_REST_Request $request)
    {
        $args = [
            'post_type' => 'promopage',
            'numberposts' => -1,
            'status' => 'publish'
        ];

        $promopages = [];
        $query = get_posts($args);

        foreach ((array)$query as $post) {
            $promopages[] = [
                'ID' => $post->ID,
                'title' => $post->post_title
            ];
        }

        if (empty($promopages)) {
            return new \WP_REST_Response([
                'message' => 'Bonus page not found'
            ], 404);
        }

        return new \WP_REST_Response([
            'data' => $promopages
        ], 200);
    }

    public function get_promopage(\WP_REST_Request $request)
    {
        $promopage_id = $request->get_param('id') ? intval($request->get_param('id')) : 0;
        $post = get_post($promopage_id);

        if ($post && $post->post_type == 'promopage') {
            $shortcodes = get_post_meta($promopage_id, 'promopage_shortcodes', true);
            $page_ids = salesloo_promopage_pages($promopage_id);

            if ($shortcodes && $page_ids) {

                $pages = [];
                foreach ($page_ids as $page_id) {
                    $path = get_the_permalink($page_id);
                    $path = str_replace(site_url(), '', $path);
                    $pages[] = [
                        'id' => intval($page_id),
                        'title' => get_the_title($page_id),
                        'path' => $path
                    ];
                }
                $promopage = [
                    'ID' => $promopage_id,
                    'title' => get_the_title($promopage_id),
                    'shortcodes' => $shortcodes,
                    'pages' => $pages
                ];

                return new \WP_REST_Response([
                    'data' => $promopage
                ], 200);
            }
        }

        return new \WP_REST_Response([
            'message' => 'Bonus page with id \'' . $promopage_id . '\' not found or not set',
        ], 404);
    }

    public function update_promopage(\WP_REST_Request $request)
    {
        $promopage_id = $request->get_param('id') ? intval($request->get_param('id')) : 0;
        $post = get_post($promopage_id);

        if ($post && $post->post_type == 'promopage') {
            $shortcodes = get_post_meta($promopage_id, 'promopage_shortcodes', true);
            $page_ids = salesloo_promopage_pages($promopage_id);

            if ($shortcodes && $page_ids) {

                $params = $request->get_body_params();
                $affiliate_username = $params['username'];
                $affiliate_shortcode = $params['shortcodes'];
                update_post_meta($promopage_id, 'promopage_shortcodes@' . sanitize_text_field($affiliate_username), $affiliate_shortcode);

                $affiliate_shortcode = get_post_meta($promopage_id, 'promopage_shortcodes@' . sanitize_text_field($affiliate_username), true);

                $pages = [];
                foreach ($page_ids as $page_id) {
                    $post = get_post($page_id);
                    if (empty($post)) continue;

                    $path = get_the_permalink($page_id);
                    $path = str_replace(site_url(), '', $path);
                    $path = rtrim($path, '/');
                    $pages[] = [
                        'id' => intval($page_id),
                        'title' => get_the_title($page_id),
                        'path' => $path . '/@' . $affiliate_username
                    ];
                }
                $promopage = [
                    'ID' => $promopage_id,
                    'title' => get_the_title($promopage_id),
                    'shortcodes' => $affiliate_shortcode,
                    'pages' => $pages
                ];

                return new \WP_REST_Response([
                    'data' => $promopage
                ], 200);
            }
        }

        return new \WP_REST_Response([
            'message' => 'Bonus page with id \'' . $promopage_id . '\' not found or not set',
        ], 404);
    }

    /**
     * construct
     */
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'register_routes']);
    }
}
