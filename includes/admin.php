<?php

namespace Salesloo_Promopage_Client;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * Admin
 */
class Admin
{

    /**
     * Instance.
     *
     * Holds the admin instance.
     *
     * @since 1.0.0
     * @access public
     */
    public static $instance = null;

    /**
     * Init.
     *
     * @since 1.0.0
     */
    public static function init()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function enqueue_scripts()
    {

        wp_enqueue_style('select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', array(), NULL, 'all');

        wp_enqueue_script('select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', array('jquery'), NULL, false);
        wp_enqueue_media();
        wp_enqueue_script('salesloo-promopage', SPPC_URL . 'assets/js/admin.js', array('jquery'), SPPC_VERSION, false);
    }

    /**
     * Register Menu
     * 
     * register system info menu
     */
    public function register_menu()
    {
        add_menu_page(
            __('Salesloo Promopage', 'salesloo-promopage'),
            __('Salesloo Promopage', 'salesloo-promopage'),
            'manage_options',
            'promopage',
            '',
            '',
            30
        );
    }


    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        add_action('admin_menu', [$this, 'register_menu']);
        add_action('admin_enqueue_scripts', [$this, 'enqueue_scripts']);
    }
}
