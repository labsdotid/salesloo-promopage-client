(function ($) {
    'use strict';

    $(function () {
        setTimeout(function () {
            let sabcGutenbergButton = $('#sabc-guttenberg-button').html();

            $('#editor').find('.edit-post-header-toolbar').append(sabcGutenbergButton);
        }, 1);

        let sabcClassicButton = $('#sabc-classic-button').html();
        console.log(sabcClassicButton);
        console.log('spadadad');

        $("body.post-type-page .wrap h1").append(sabcClassicButton);

    });



})(jQuery);

let sabcImageFrame;

function sabcUploader(ini) {

    let td = jQuery(ini).parent();
    let field = td.find('input');

    // Sets up the media library frame
    sabcImageFrame = wp.media.frames.sabcImageFrame = wp.media({
        title: 'Upload Image',
        button: { text: 'Use this file' },
    });

    // Runs when an image is selected.
    sabcImageFrame.on('select', function () {

        // Grabs the attachment selection and creates a JSON representation of the model.
        var media_attachment = sabcImageFrame.state().get('selection').first().toJSON();

        // Sends the attachment URL to our custom image input field.
        field.val(media_attachment.url);

    });

    // Opens the media library frame.
    sabcImageFrame.open();
}
